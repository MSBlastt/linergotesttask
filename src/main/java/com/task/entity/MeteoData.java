package com.task.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(schema = "public", name = "meteo_data")
public class MeteoData {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true)
	private Long id;
	@Column(name="meteo_station_id")
	private Long meteo_station_id;
	@Column(name="read_timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date read_timestamp;
	@Column(name="temperature")
	private BigDecimal temperature;
	@Column(name="pressure")
	private Integer pressure;
	@Column(name="wind_direction")
	private Integer wind_direction;
	@Column(name="wind_speed")
	private Integer wind_speed;

	public void setMeteo_station_id(Long meteo_station_id) {
		this.meteo_station_id = meteo_station_id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public Long getMeteo_station_id() {
		return meteo_station_id;
	}

	public Date getRead_timestamp() {
		return read_timestamp;
	}

	public void setRead_timestamp(Date read_timestamp) {
		this.read_timestamp = read_timestamp;
	}

	public BigDecimal getTemperature() {
		return temperature;
	}

	public void setTemperature(BigDecimal temperature) {
		this.temperature = temperature;
	}

	public Integer getPressure() {
		return pressure;
	}

	public void setPressure(Integer pressure) {
		this.pressure = pressure;
	}

	public Integer getWind_direction() {
		return wind_direction;
	}

	public void setWind_direction(Integer wind_direction) {
		this.wind_direction = wind_direction;
	}

	public Integer getWind_speed() {
		return wind_speed;
	}

	public void setWind_speed(Integer wind_spped) {
		this.wind_speed = wind_spped;
	}
}
