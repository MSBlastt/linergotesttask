package com.task.entity;

import javax.persistence.*;

@Entity
@Table(schema = "public", name = "meteo_station")
public class MeteoStation {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", unique = true)
        private Long id;
        @Column(name="name")
        private String name;
        @Column(name="address")
        private String address;

    public MeteoStation() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
