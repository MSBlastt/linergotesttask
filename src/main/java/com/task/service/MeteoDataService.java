package com.task.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.task.entity.MeteoData;

import java.util.Collection;

@Component
public class MeteoDataService {
	@PersistenceContext
	private EntityManager em;

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	@Transactional
	public void register(MeteoData meteoData) {
		this.em.persist(meteoData);
	}

	@Transactional
	public void update(MeteoData meteoData) {
		this.em.merge(meteoData);
	}

	public Collection<MeteoData> getAllMeteoData() {
		Query query = em.createQuery("SELECT m FROM MeteoData m");
		return (Collection<MeteoData>) query.getResultList();
	}

}
