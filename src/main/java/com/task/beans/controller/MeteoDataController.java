package com.task.beans.controller;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import com.task.entity.MeteoData;
import com.task.service.MeteoDataService;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.primefaces.event.RowEditEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

@ManagedBean
@SessionScoped
public class MeteoDataController {

	@ManagedProperty("#{meteoDataService}")
	private MeteoDataService meteoDataService;

	@PostConstruct
	public void init() {
		list = meteoDataService.getAllMeteoData();
	}

	private Collection<MeteoData> list;

	private MeteoData meteoData = new MeteoData();

	public MeteoDataService getMeteoDataService() {
		return meteoDataService;
	}

	public void setMeteoDataService(MeteoDataService meteoDataService) {
		this.meteoDataService = meteoDataService;
	}

	public MeteoData getMeteoData() {
		return meteoData;
	}

	public void setMeteoData(MeteoData meteoData) {
		this.meteoData = meteoData;
	}

	public Collection<MeteoData> getList() {
		return list;
	}


	public String register() throws IOException, InterruptedException {
		meteoDataService.register(meteoData);
		list = meteoDataService.getAllMeteoData();
		FacesContext.getCurrentInstance().addMessage(null, 
				new FacesMessage("Метеозапись под номером " + this.meteoData.getId() + " была успешно добавлена."));
		return "";
	}

	public void onRowEdit(RowEditEvent event) {
		meteoDataService.update((MeteoData) event.getObject());
		FacesMessage msg = new FacesMessage("Метеоданные обновлены", "Метеозапись № " + String.valueOf(((MeteoData) event.getObject()).getId()));
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void onRowCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("Изменения отменены", "Метеозапись № " + String.valueOf(((MeteoData) event.getObject()).getId()));
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public StreamedContent generateJson () throws IOException {
		String json = "";
		for (MeteoData md : list) {
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			 json += ow.writeValueAsString(md);
		}
		InputStream stream = new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8));
		return new DefaultStreamedContent(stream, "json", "report.json");
	}
}
