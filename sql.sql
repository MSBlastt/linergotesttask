CREATE TABLE public.meteo_data
(
    id SERIAL PRIMARY KEY NOT NULL,
    meteo_station_id BIGINT NOT NULL,
    read_timestamp TIMESTAMP NOT NULL,
    temperature DECIMAL(4,1) NOT NULL,
    pressure INT NOT NULL,
    wind_direction INT NOT NULL,
    wind_speed INT NOT NULL
);
ALTER TABLE public.meteo_data
 ADD CONSTRAINT unique_id UNIQUE (id);	

CREATE TABLE public.meteo_station
(
    id SERIAL PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    address TEXT NOT NULL
);
ALTER TABLE public.meteo_station
 ADD CONSTRAINT unique_id UNIQUE (id);